package controllers;

import models.*;
import play.data.Form;
import play.data.FormFactory;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import javax.inject.Inject;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;
import javax.management.Query;
import javax.persistence.*;
import play.db.jpa.JPAApi;
import java.beans.Transient;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import play.mvc.*;
import play.mvc.Http.MultipartFormData;
import play.mvc.Http.MultipartFormData.FilePart;
import com.cloudinary.*;
import com.cloudinary.utils.ObjectUtils;
import java.io.File;
import java.util.Map;

public class ProductController extends Controller{

    /**
     * Inject tthe FormFactory necesario para Crear los Formularios
     */
    @Inject
    FormFactory fm;

    /**
     * Inject the JPAApi necesario para utilizar el EntityManager
     */
    @Inject
    JPAApi api;

    /**
     * Metodo que se encarga de generar el formulario para la creacion de un Producto
     * @param nombreSucursal Nombre de la sucursal a la cual pertenece el producto
     * @return Una respuesta HTTP 200, que mostrara el formulario necesario para la creacion del producto
     */
    @Transactional(readOnly=true)
    public Result crearProducto(String nombreSucursal){
        Form<Product> form = fm.form(Product.class).bindFromRequest();
        return ok(create_product.render(form,nombreSucursal,""));
    }

    /**
     * Metodo que se encarga de agregar a la base de datos un Producto con los datos del formulario de creacion de producto
     * y Crea el registro propio de este.
     * @param nombreBodega Nombre de la sucursal a la cual pertenece el producto
     * @return Una respuesta HTTP 200, que redirecciona a la informacion de la bodega en caso de ser existoso
     * de lo contrario envia una respuesta HTTP 400, que muestra el formulario de creacion del Producto con el error.
     */
    @Transactional
    public Result addProducto(String nombreBodega){
        EntityManager em = api.em();
        Bodega b = em.find(Bodega.class, nombreBodega);
        em.flush();
        Form<Product> form = fm.form(Product.class).bindFromRequest();
        String nombre = form.data().get("nombre");
        for(Product nombreProducto: b.getProductos()){
            if(nombreProducto.getNombre().equalsIgnoreCase(nombre)){
                return badRequest(create_product.render(form, nombreBodega,"Ya existe un Producto con este nombre en la Sucursal"));
            }
        }
        try{
            int codigo = Integer.parseInt(form.data().get("codVenta"));
            for(Product pro : listarProductos()){
                if(pro.getCodVenta() == codigo){
                    return badRequest(create_product.render(form,nombreBodega,"Ya Existe un Producto con este Codigo"));
                }
            }
            if(codigo < 0){
                return badRequest(create_product.render(form,nombreBodega,"No Pueden Tener numeros negativos en el codigo"));
            }
            String marca = form.data().get("marca");
            String proveedor = form.data().get("proveedor");
            try{
                int stock = Integer.parseInt(form.data().get("stock"));
                if(stock < 0){
                    return badRequest(create_product.render(form,nombreBodega,"El stock no puede tener numeros Negativos"));
                }
                String fecha = form.data().get("verificacion").toString();
                if(!fechavalida(fecha)){
                    return badRequest(create_product.render(form,nombreBodega,"La fecha no es valida intente con un formato 'YYYY-MM-DD'"));
                }
                MultipartFormData<File> body = request().body().asMultipartFormData();
                FilePart<File> productFilePart = body.getFile("urlFoto");
                if(productFilePart != null){
                    String fileName = productFilePart.getFilename();
                    if(fileName.endsWith(".jpg") ||fileName.endsWith(".JPG") || fileName.endsWith(".png") || fileName.endsWith(".PNG")
                            || fileName.endsWith(".jpeg") || fileName.endsWith(".JPEG")){
                        String contentType = productFilePart.getContentType();
                        File file = productFilePart.getFile();
                        Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                                "cloud_name", "crishegar",
                                "api_key", "321276547173215",
                                "api_secret", "z4GZBATt5q7NFB-IwQobDr2Slcc"));
                        try{
                            Map uploadResult = cloudinary.uploader().upload(file, ObjectUtils.asMap(
                                    "resource_type","auto"
                            ));
                            Product product = new Product(nombre, codigo, marca,proveedor,stock,fecha,uploadResult.get("url").toString(),b);

                            int codReg = totalRegistros().size()+1;
                            Registro reg = new Registro(codReg, codigo, nombre, stock,fecha,"Producto Agregado", b);
                            em.persist(reg);
                            em.persist(product);
                            return redirect(routes.BodegaController.show(nombreBodega));
                        }catch(Exception e){
                            e.printStackTrace();
                            return badRequest(e.getMessage() + "Expltotamos aca");
                        }
                    }else{
                        return badRequest(create_product.render(form,nombreBodega,"El formato del archivo debe ser o .jpg, png, jpeg"));
                    }
                }else {
                    return badRequest(create_product.render(form,nombreBodega,"No ha seleccionado Un archivo"));
                }
            }catch(Exception e){
                return badRequest(create_product.render(form,nombreBodega,"El Stock del producto no Tiene formato Valido"));
            }
        }catch(Exception e){
            return badRequest(create_product.render(form,nombreBodega,"El codigo del producto no es Valido"));
        }
    }

    /**
     * MEtodo encargado de editar un producto existente con los datos obtenidos por el formulario EditarProducto
     * y Crea un registro de esta modificacion.
     * @param nombreSucursal Nombre de la sucursal a la cual pertenece el prooducto
     * @param codVenta Codigo del producto que se desea editar
     * @return una respuesta HTTP 200, la cual redirigue a mostrar los datos de la Sucursal
     * en caso de error genera una respuesta HTTP 400, que envia al formulario de edicion del producto y muestra el error
     */
    @Transactional
    public Result uploadProduct(String nombreSucursal, int codVenta){
        EntityManager em = api.em();
        Product p = em.find(Product.class, codVenta);
        Bodega b = em.find(Bodega.class, nombreSucursal);
        Form <Product> form = fm.form(Product.class).bindFromRequest();
        int stock = Integer.parseInt(form.data().get("stock").toString());
        if (stock < 0){
            return badRequest(edit_Product.render (form,p,"El Stock no puede tener numeros negativos"));
        }else{
            p.setStock(stock);
        }
        String nombre = form.data().get("nombre");
        if (nombre != null && nombre != ""){
            if(nombre.equalsIgnoreCase(p.getNombre())){
                p.setNombre(nombre);
            }else{
                for(Product pb: b.getProductos()){
                    if(pb.getNombre().equalsIgnoreCase(nombre)){
                        return badRequest(edit_Product.render(form,p,"Ya Existe un Producto con este nombre en la sucursal"));
                    }
                }
                p.setNombre(nombre);
            }
        }
        String marca = form.data().get("marca");
        if(marca != null && marca != ""){
            p.setMarca(marca);
        }
        String proveedor = form.data().get("proveedor");
        if(proveedor != null && proveedor != ""){
            p.setProveedor(proveedor);
        }
        String fecha = form.data().get("verificacion");
        if(fecha != null && fecha != "" && fechavalida(fecha)){
            String antFecha = p.getVerificacion();
            String[] partaf = antFecha.split("-");
            try{
                String[] partf = fecha.split("-");
                if(Integer.parseInt(partaf[0])> Integer.parseInt(partf[0])){
                    return badRequest(edit_Product.render(form,p,"No puede colocar una fecha anterior a " + antFecha));
                }else if(Integer.parseInt(partaf[1])> Integer.parseInt(partf[1])){
                    return badRequest(edit_Product.render(form,p,"No puede colocar una fecha anterior a " + antFecha));
                }else if(Integer.parseInt(partaf[1])> Integer.parseInt(partf[2])){
                    return badRequest(edit_Product.render(form,p,"No puede colocar una fecha anterior a " + antFecha));
                }else{
                    p.setVerificacion(fecha);
                }
            }catch (Exception e){
                return badRequest(edit_Product.render(form,p,"El formato de la fecha es incorrecto"));
            }
        }
        int codReg = totalRegistros().size() + 1;
        Registro reg = new Registro(codReg, p.getCodVenta(), p.getNombre(),p.getStock(),p.getVerificacion(),"Producto Editado",p.getBodega());
        em.persist(reg);
        return redirect(routes.BodegaController.show(b.getNombreSucursal()));
    }

    /**
     * MEtodo que se encarga de crear el Formulario para la edicion de un producto existente
     * @param nombreSucursal Nombre de la sucursal a la cual pertenece el producto
     * @param codVenta Codigo del producto Existente
     * @return Retorna una respuesta HTTP 200, la cual muestra el Formulario de Edicion de un Producto
     */
    @Transactional(readOnly=true)
    public Result edit(String nombreSucursal, int codVenta){
        EntityManager em = api.em();
        Product p = em.find(Product.class, codVenta);
        Form<Product> form = fm.form(Product.class).bindFromRequest();
        return ok(edit_Product.render(form, p,""));
    }

    /**
     * Metodo encargado de Eliminar un Producto Existente y generar un Registro de que se ha borrado el producto
     * @param nbodega Nombre de la Sucursal a la cual pertenece el producto
     * @param codVenta Codigo Unico del producto que se desea eliminar
     * @return
     */
    @Transactional
    public Result delete(String nbodega, int codVenta){
        EntityManager em = api.em();
        Bodega bodega = em.find(Bodega.class, nbodega);
        for(Product p: bodega.getProductos()){
            if(p.getCodVenta()==codVenta){
                Product pr = em.find(Product.class, codVenta);
                int codReg = totalRegistros().size()+1;
                Registro reg = new Registro(codReg, p.getCodVenta(), p.getNombre(), p.getStock(),p.getVerificacion(),"Producto Borrado", p.getBodega());
                em.persist(reg);
                em.remove(pr);
                return redirect(routes.BodegaController.show(nbodega));
            }
        }
        return badRequest("No Existe eL producto a borrar");
    }

    /**
     * Metodo que se encarga de crear el formulario para cambiar la foto de un producto
     * @param nombreSucursal Nombre de la sucursal a la cual pertenece el producto
     * @param codVenta codigo unico del producto al cual se le efectua el cambio de foto
     * @return una respuesta HTTP 200, la cual visualiza el formulario de cambio de foto
     */
    @Transactional(readOnly=true)
    public Result cambiarFoto(String nombreSucursal, int codVenta){
        Form<Product> form = fm.form(Product.class).bindFromRequest();
        EntityManager em = api.em();
        Product p = em.find(Product.class, codVenta);
        return ok(cambiar_foto.render(form,p));
    }

    /**
     * Metodo que se encarga de recibir la foto por la cual se realizara el cambio
     * @param nombreSucursal Nombre de la sucursal a la que pertence el producto
     * @param codVenta Codigo unico del producto al cual se le efectuara el cambio
     * @return Una respuesta HTTP 200,La cual realizara el cambio, y nos enviara de vuelta a visualizar la sucursal
     * En caso de error retornara una respuesta HTTP 400, la cual devolvera al formulario mostrando el error
     */
    @Transactional
    public Result uploadFoto(String nombreSucursal, int codVenta){
        EntityManager em = api.em();
        Product p = em.find(Product.class, codVenta);
        Form<Product> form = fm.form(Product.class).bindFromRequest();
        MultipartFormData<File> body = request().body().asMultipartFormData();
        FilePart<File> productFilePart = body.getFile("urlFoto");
        if(productFilePart != null){
            String fileName = productFilePart.getFilename();
            if(fileName.endsWith(".jpg") ||fileName.endsWith(".JPG") || fileName.endsWith(".png") || fileName.endsWith(".PNG")
                    || fileName.endsWith(".jpeg") || fileName.endsWith(".JPEG")){
                String contentType = productFilePart.getContentType();
                File file = productFilePart.getFile();
                Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                        "cloud_name", "crishegar",
                        "api_key", "321276547173215",
                        "api_secret", "z4GZBATt5q7NFB-IwQobDr2Slcc"));
                try{
                    Map uploadResult = cloudinary.uploader().upload(file, ObjectUtils.asMap(
                            "resource_type","auto"
                    ));
                    p.setUrlfoto(uploadResult.get("url").toString());
                    return redirect(routes.BodegaController.show(nombreSucursal));
                }catch(Exception e){
                    e.printStackTrace();
                    return badRequest(e.getMessage() + "Expltotamos aca");
                }
            }else{
                form.reject("urlFoto", "Formato no valido");
                return badRequest(cambiar_foto.render(form,p));
            }
        }else {
            return badRequest(cambiar_foto.render(form,p));
        }
    }

    /**
     * Metodo que se encarga de listar los productos existentes
     * @return una lista de todos los productos.
     */
    @Transactional(readOnly=true)
    public List<Product> listarProductos(){
        EntityManager em = api.em();
        TypedQuery <Product> sql = em.createQuery("SELECT p FROM Product p", Product.class);
        List<Product> productos = sql.getResultList();
        return productos;
    }

    /**
     * Metodo que se encarga de listar todos los registros existentes
     * @return una lista con todos los Productos existentes
     */
    @Transactional(readOnly=true)
    public List<Registro> totalRegistros(){
        EntityManager em = api.em();
        TypedQuery<Registro> sql = em.createQuery("Select r FROM Registro r", Registro.class);
        List<Registro> registros = sql.getResultList();
        return registros;
    }

    /**
     * Metodo que se encarga de validar que la fecha tenga un formato yyyy-mm-dd (Metodo sacado de stackoverflow.com)
     * @param fecha Fecha que va  aser validada
     * @return Verdadero, si la fecha cumple el formato.
     */
    public boolean fechavalida(String fecha) {
        if (fecha == null || !fecha.matches("\\d{4}-[01]\\d-[0-3]\\d"))
            return false;
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        df.setLenient(false);
        try {
            df.parse(fecha);
            return true;
        } catch (ParseException ex) {
            return false;
        }
    }

}
