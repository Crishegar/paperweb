package controllers;

import models.*;
import play.data.Form;
import play.data.FormFactory;
import play.mvc.Controller;
import play.db.jpa.JPAApi;
import play.db.jpa.JPA;
import play.mvc.Result;
import javax.inject.Inject;
import java.util.List;
import views.html.*;
import play.db.jpa.Transactional;
import javax.persistence.*;

public class BodegaController extends Controller{

    /**
     * Inject tthe FormFactory necesario para Crear los Formularios
     */
    @Inject
    FormFactory fm;

    /**
     * Inject the JPAApi necesario para utilizar el EntityManager
     */
    @Inject
    JPAApi api;

    /**
     * MEtodo utilizado para mostrar la lista de Bodegas en la Vista "Home o Inicio"
     * @return La vista Inicial de la aplicacion
     */
    @Transactional(readOnly=true)
    public Result index(){
     List<Bodega> bodegas = listBodegas();
     return ok(index.render(bodegas));
    }

    /**
     * MEtodo que se encarga de Crear el Formulario necesario para La creacion de bodegass
     * @return una respuesta http 200, Que muestra el formulario de creaccion de bodegas
     */
    public Result createBodega(){
        Form<Bodega> form = fm.form(Bodega.class).bindFromRequest();
        return ok(create_bodega.render(form,""));
    }

    /**
     * MEtodo que se encarga de obtener los datos del formulario de creacion de bodegas, verifica los datos y de ser existoso Agrega a la base
     * de datos la bodega.
     * @return una respuesta HTTP 200, que redirecciona a la vistaa de inicio, en caso de error retorna HTTP 400 el cual redirecciona
     * al formulario, y muestra el error.
     */
    @Transactional
    public Result addBodega(){
        EntityManager em = api.em();
        Form<Bodega> form = fm.form(Bodega.class).bindFromRequest();
        Bodega b = form.get();
        List<Bodega> bodegas = listBodegas();
        for(Bodega bodega: bodegas){
            if(bodega.getNombreSucursal().equals(b.getNombreSucursal())){
                return badRequest(create_bodega.render(form,"Ya Existe una Bodega con ese Nombre"));
            }
        }
        em.persist(b);
        return redirect(controllers.routes.BodegaController.index());
    }

    /**
     * Metdo encargado de mostrar los datos de una bodega
     * @param bodega el nombre de la bodega, de la cual se mostraran los datos
     * @return una respuesta HTTP 200, que muestra la informacion de la bodega, en caso de error retorna
     * HTTP 400, con el error "no busque una bodega que no existe"
     */
    @Transactional(readOnly=true)
    public Result show(String bodega){
        Bodega b = api.em().find(Bodega.class, bodega);
        if(b == null){
            return badRequest("No busque una bodega que no existe");
        }else{
            return ok(show.render(b));
        }
    }

    /**
     * Metodo que se encarga de Mostrar en una vista el Documento
     * @param nombreBodega
     * @return
     */
    @Transactional
    public Result reporte(String nombreBodega){
        EntityManager em = api.em();
        Bodega b = em.find(Bodega.class, nombreBodega);
        return ok(reporte.render(b));
    }

    /**
     *Metodo que se encarga de buscar las bodegas existentes
     * @return una lista de bodegas existentes.
     */
    @Transactional(readOnly=true)
    public List<Bodega> listBodegas(){
        EntityManager em = api.em();
        TypedQuery <Bodega> sql = em.createQuery("SELECT b FROM Bodega b", Bodega.class);
        List<Bodega> bodegas = sql.getResultList();
        return bodegas;
    }
}
