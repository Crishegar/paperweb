package models;

import java.util.List;
import javax.persistence.*;
import models.*;

/**
 * Clase que modela la tabla Bodega de la base de datos
 */
@Entity
@Table(name="bodega")
public class Bodega {

    /**
     * Atributo que modela el campo nombre de la bodega
     */
    @Id
    @Column(name="nombre")
    private String nombreSucursal;

    /**
     * Atributo que modela el campo encargado de la tabla bodega
     */
    @Column(name="nombreResponsable")
    private String encargado;

    /**
     * Atributo que establece la relacion uno a muchos con la tabla Producto y contiene los productos de la bodega
     */
    @OneToMany(mappedBy="bodega")
    private List<Product> productos;

    /**
     * Atributo que establece la relacion uno a muchos con la tabla Registro y contiene los registros de la bodega
     */
    @OneToMany(mappedBy="bodega")
    private List<Registro> registros;

    /**
     * Metodo que inicializa una Bodega con atributos vacios
     */
    public Bodega(){

    }

    /**
     * Metodo que instancia una bodega con todos sus atributos
     * @param nombreSucursal El nombre de la sucursal a la cual pertenece la Bodega
     * @param encargado Nombre del encargado de la sucursal
     */
    public Bodega(String nombreSucursal, String encargado){
        this.nombreSucursal = nombreSucursal;
        this.encargado = encargado;
    }

    /**
     * Metodo que obtiene el nombre del encargado
     * @return El nombre del encargado
     */
    public String getEncargado() {
        return encargado;
    }

    /**
     * Metodo que modifica el nombre del encargado
     * @param encargado nombre del nuevo encargado
     */
    public void setEncargado(String encargado) {
        this.encargado = encargado;
    }

    /**
     * Metodo que obtiene el nombre de la sucursal
     * @return el nombre de la sucursal
     */
    public String getNombreSucursal() {
        return nombreSucursal;
    }

    /**
     * Metodo que modifica el nombre de la sucursal
     * @param nombreSucursal nombre de la sucursal por la cual se desea cambiar el actual
     */
    public void setNombreSucursal(String nombreSucursal) {
        this.nombreSucursal = nombreSucursal;
    }

    /**
     * Metodo que obtiene la lista de productos de la sucursal
     * @return Lista de los productos de la sucursal
     */
    public List<Product> getProductos() {
        return productos;
    }

    /**
     * Metodo que obtiene la lista de registros de la sucursal
     * @return la lista de registros de la Sucursal
     */
    public List<Registro> getRegistros() {
        return registros;
    }

    @Override
    public String toString() {
        return "Bodega de sucursal = " + nombreSucursal +" " + productos.size();
    }
}
