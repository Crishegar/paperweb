package models;

import java.util.Date;
import javax.persistence.*;
import models.*;

/**
 * Metodo que Modela la tabla Producto de la base de datos
 */
@Entity
@Table(name="producto")
public class Product {

    /**
     * Atributo que modela el nombre del producto
     */
    @Column(name="nombreProducto")
    private String nombre;

    /**
     * Atributo que modela el codigo unico del producto
     */
    @Id
    @Column(name="codigoventa")
    private int codVenta;

    /**
     * Atributo que modela la marca del producto
     */
    @Column(name="marca")
    private String marca;

    /**
     * Atributo que modela el proveedor del producto
     */
    @Column(name="proveedor")
    private String proveedor;

    @Column(name="stock")
    private int stock;

    /**
     * Atributo que modela la cantidad disponible en bodega del producto
     */
    @Column(name="ultimaVerificacion")
    private String verificacion;

    /**
     * MEtodo que modela la direccion de la imagen del producto
     */
    @Column(name="urlFoto")
    private String urlfoto;

    /**
     * MEtodo que modela la bodega en la cual se almacena el producto
     */
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="nombreSucursal")
    private Bodega bodega;

    /**
     * Metodo que instancia el producto con sus campos vacios
     */
    public Product() {

    }

    /**
     * Metodo que instancia el producto con todos los campos
     * @param nombre nombre del producto
     * @param codVenta codigo unico del producto
     * @param marca marca del producto
     * @param proveedor proveedor del producto
     * @param stock cantidad disponible en bodega del producto
     * @param verificacion Fecha de la ultima modificacion del producto
     * @param urlfoto Direccion de la imagen del producto
     * @param bodega Bodega en la cual esta almacenado el producto
     */
    public Product(String nombre, int codVenta, String marca, String proveedor, int stock, String verificacion, String urlfoto, Bodega bodega) {
        this.nombre = nombre;
        this.codVenta = codVenta;
        this.marca = marca;
        this.proveedor = proveedor;
        this.stock = stock;
        this.verificacion = verificacion;
        this.urlfoto = urlfoto;
        this.bodega = bodega;
    }

    /**
     * Metodo que obtiene el nombre del producto
     * @return el nombre del producto
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo que modifica el nombre del producto por uno nuevo ingresado por parametro
     * @param nombre nombre por el cual se va a modificar el actual
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo que obtiene el codigo unico del producto
     * @return el codigo unico del producto
     */
    public int getCodVenta() {
        return codVenta;
    }

    /**
     * Metodo que modifica el codigo unico del producto por uno ingresado por parametro
     * @param codVenta codigo por el cual se va a modificar el actual
     */
    public void setCodVenta(int codVenta) {
        this.codVenta = codVenta;
    }

    /**
     * Metodo que obtiene la marca del producto
     * @return marca del producto
     */
    public String getMarca() {
        return marca;
    }

    /**
     * Metodo que modifica la marca del producto por una ingresada por parametro
     * @param marca marca por la cual se va a modificar la actual
     */
    public void setMarca(String marca) {
        this.marca = marca;
    }

    /**
     * Metodo que obtiene el proveedor del producto
     * @return el proveedor del producto
     */
    public String getProveedor() {
        return proveedor;
    }

    /**
     * Metodo que modifica el proveedor del producto por uno ingresado por parametro
     * @param proveedor Proveedor por el cual se va a modificar el actual
     */
    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * Metodo que obtiene la cantidad disponible en bodega del producto
     * @return la cantidad disponible en bodega del producto
     */
    public int getStock() {
        return stock;
    }

    /**
     * MEtodo por el cual se modifica la cantidad disponible en bodega por la ingresada por parametro
     * @param stock cantidad disponible en bodega por la cual se va a modificar la actual
     */
    public void setStock(int stock) {
        this.stock = stock;
    }

    /**
     * Metodo que obtiene la fecha de ultima modificacion del producto
     * @return la fecha de ultima modificaicon del producto
     */
    public String getVerificacion() {
        return verificacion;
    }

    /**
     * Metodo por el cual se modifica la fecha de ultima verificacion del producto por la ingresada por parametro
     * @param verificacion fecha por la cual se va a modificar la actual
     */
    public void setVerificacion(String verificacion) {
        this.verificacion = verificacion;
    }

    /**
     * Metodo que obtiene la url de la imagen del producto
     * @return la url de la imagen del producto
     */
    public String getUrlfoto() {
        return urlfoto;
    }

    /**
     * Metodo por el cual se modifica la url de la imagen del producto por la ingresada por parametro
     * @param urlfoto url de la imagen por la cual se va a modificar la actual
     */
    public void setUrlfoto(String urlfoto) {
        this.urlfoto = urlfoto;
    }

    /**
     * Metodo que obtiene la bodega en la cual se encuentra almacenado del producto
     * @return la bodega en la cual esta almacenado el producto
     */
    public Bodega getBodega() {
        return bodega;
    }

    @Override
    public String toString() {
        return "Producto: " + codVenta + " " + nombre + " en bodega: " + bodega;
    }
}
