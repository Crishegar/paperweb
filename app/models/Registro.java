package models;

import java.util.Date;
import javax.persistence.*;
import models.*;

/**
 * Clase que modela la tabla Registro de la base de datos
 */
@Entity
@Table(name="registro")
public class Registro {

    /**
     * Atributo que modela el Codigo del registro
     */
    @Id
    @Column(name="codRegistro")
    private int codRegistro;

    /**
     * Atributo que modela el codigo del producto
     */
    @Column(name="codProd")
    private int codProd;

    /**
     * Atributo que modela el nombre del producto
     */
    @Column(name="nombreProd")
    private String nombreProd;

    /**
     * Atributo que modela la cantidad disponible de productos en la sucursal
     */
    @Column(name="stockProd")
    private int stockProd;

    /**
     * Atributo que modela la fecha en la cual fue modificado por ultima vez el producto de la sucursal
     */
    @Column(name="fechaProd")
    private String fechaProd;

    /**
     * Atributo que modela la razon por la cual fue tomada el registro
     */
    @Column(name="razon")
    private String razon;

    /**
     * Atributo que modela la bodega a la cual pertenecen los registros
     */
    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="sucursal")
    private Bodega bodega;

    /**
     * Metodo que instancia un registro con sus atributos vacios
     */
    public Registro() {
    }

    /**
     * Metodo que instancia un registro con todos los campos
     * @param codRegistro Codigo unico del Registro
     * @param codProd Codigo unico del producto al que se le toma el registro
     * @param nombreProd Nombre del producto al que se le toma el registro
     * @param stockProd Cantidad disponible del producto al que se le toma el registro
     * @param fechaProd Fecha de la ultima modificacion de los registros del producto
     * @param razon Razon po la cual fue realizado el Registro
     * @param bodega Bodega a la cual pertenece el registro
     */
    public Registro(int codRegistro,int codProd, String nombreProd, int stockProd, String fechaProd, String razon, Bodega bodega) {
        this.codRegistro = codRegistro;
        this.codProd = codProd;
        this.nombreProd = nombreProd;
        this.stockProd = stockProd;
        this.fechaProd = fechaProd;
        this.razon = razon;
        this.bodega = bodega;
    }

    /**
     * Metodo que obtiene el codigo unico del registro
     * @return El codugo del registro
     */
    public int getCodRegistro() {
        return codRegistro;
    }

    /**
     * MEtodo que obtiene el codigo unico del producto al que se le realiza el registro
     * @return El codigo del producto registrado
     */
    public int getCodProd() {
        return codProd;
    }

    /**
     * Metodo que retorna el nombre del producto al que se le realiza el registro
     * @return El nombre del producto registrado
     */
    public String getNombreProd() {
        return nombreProd;
    }

    /**
     * Metodo que obtiene la cantidad disponible en el momento del producto registrado
     * @return la cantidad del producto registrado
     */
    public int getStockProd() {
        return stockProd;
    }

    /**
     * Metodo que obtiene la fecha de ultima actualizacion del producto registrado
     * @return fecha de la ultima actualizacion del producto registrado
     */
    public String getFechaProd() {
        return fechaProd;
    }

    /**
     * Metodo que obtiene la bodega a la cual pertenecen los registros
     * @return la bodega a la que pertenecen los registros
     */
    public Bodega getBodega() {
        return bodega;
    }

    /**
     *Metodo que obtiene la Razon por la que se realizo el regitro
     * @return razon del registro
     */
    public String getRazon() {
        return razon;
    }
}
