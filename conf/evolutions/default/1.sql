
# --- !Ups

CREATE TABLE bodega(nombre character varying(50) NOT NULL,
	nombreResponsable character varying(50) NOT NULL,
	CONSTRAINT PK_BODEGA PRIMARY KEY (nombre)
);

CREATE TABLE producto(codigoventa integer NOT NULL,
	nombreProducto character varying(50) NOT NULL,
	marca character varying(50) NOT NULL,
	proveedor character varying(50) NOT NULL,
	stock integer NOT NULL,
	ultimaVerificacion character varying(50) NOT NULL,
	nombreSucursal character varying(50) NOT NULL,
	urlFoto character varying(100) NOT NULL,
	CONSTRAINT PK_PRODUCTO PRIMARY KEY (codigoventa),
	CONSTRAINT FK_PRODUCTO_BODEGA FOREIGN KEY (nombreSucursal) REFERENCES bodega(nombre)
);

CREATE TABLE registro(codRegistro integer NOT NULL,
codProd integer NOT NULL,
nombreProd character varying (50) NOT NULL,
stockProd integer NOT NULL,
fechaProd character varying(50) NOT NULL,
razon character varying (100) NOT NULL,
sucursal character varying (50) NOT NULL,
CONSTRAINT PK_REGISTRO PRIMARY KEY (codRegistro),
CONSTRAINT FK_REGISTRO_PRODUCT FOREIGN KEY (sucursal) REFERENCES bodega(nombre)
);

# --- !Downs

DROP TABLE IF EXIST producto, registro, bodega CASCADE;

