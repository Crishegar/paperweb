name := """paperweb"""
organization := "Crishegar"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.2"

libraryDependencies += guice

herokuAppName in Compile := "paperweb"

herokuConfigVars in Compile := Map(
  "JDBC_DATABASE_URL" -> "jdbc:postgresql://ec2-54-227-237-223.compute-1.amazonaws.com:5432/d9tevacsj7459s?user=romsooqormcgpx&password=3a5ac5c18dd4a4d23d37ea5d0656391985cbcc78b62fb97085b7fd2dd7b10c8f&sslmode=require"
)

libraryDependencies ++= Seq(
  javaJdbc,
  evolutions,
  jdbc,
  javaJpa,
  "org.hibernate" % "hibernate-entitymanager" % "5.1.0.Final",
  "org.postgresql" % "postgresql" % "9.4-1206-jdbc42",
  "com.cloudinary" % "cloudinary-http44" % "1.13.0"
)

PlayKeys.externalizeResources := false