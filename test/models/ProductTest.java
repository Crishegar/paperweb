package models;

import org.junit.Test;

import static org.junit.Assert.*;

public class ProductTest {

    @Test
    public void testGets(){
        Product p = new Product("testNP",1,"testMP","testPP",10,"testVP","testUP",
                new Bodega("ts1","te1"));
        assertEquals("EL nombre del producto deberia ser: testNP",p.getNombre(),"testNP");
        assertEquals("El codigo deberia ser: 1",p.getCodVenta(),1);
        assertEquals("La marca deberia ser: testMP",p.getMarca(),"testMP");
        assertEquals("El proveedor deberia ser: testPP",p.getProveedor(),"testPP");
        assertEquals("El stock deberia ser: 10",p.getStock(),10);
        assertEquals("La verificacion deberia ser: testVP",p.getVerificacion(),"testVP");
        assertEquals("La url deberia ser: testUP",p.getUrlfoto(),"testUP");
    }

    public void testSets(){
        Product p = new Product();
        p.setNombre("testNP");
        p.setCodVenta(1);
        p.setMarca("testMP");
        p.setProveedor("testPP");
        p.setStock(10);
        p.setVerificacion("testVP");
        p.setUrlfoto("testUP");
        assertEquals("EL nombre del producto deberia ser: testNP",p.getNombre(),"testNP");
        assertEquals("El codigo deberia ser: 1",p.getCodVenta(),1);
        assertEquals("La marca deberia ser: testMP",p.getMarca(),"testMP");
        assertEquals("El proveedor deberia ser: testPP",p.getProveedor(),"testPP");
        assertEquals("El stock deberia ser: 10",p.getStock(),10);
        assertEquals("La verificacion deberia ser: testVP",p.getVerificacion(),"testVP");
        assertEquals("La url deberia ser: testUP",p.getUrlfoto(),"testUP");
    }

}
